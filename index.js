import { AppRegistry } from 'react-native';
import App from './src/App';

AppRegistry.registerComponent('trivia_game', () => App);
