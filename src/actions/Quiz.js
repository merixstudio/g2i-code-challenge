import Axios from 'axios';
import * as types from '../types/Quiz';

const fetchQuestionsBegin = () => ({
  type: types.QUIZ_QUESTIONS_FETCH_REQUESTED,
});

const fetchQuestionsFailure = () => ({
  type: types.QUIZ_QUESTIONS_FETCH_FAILURE,
});

const fetchQuestionsSuccessful = ({ results }) => ({
  type: types.QUIZ_QUESTIONS_FETCH_SUCCESSFUL,
  payload: results,
});

export const fetchQuizQuestions = () => (dispatch) => {
  dispatch(fetchQuestionsBegin());

  return Axios.get('https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean')
    .then(response => response.data)
    .then(data => dispatch(fetchQuestionsSuccessful(data)))
    .catch(() => fetchQuestionsFailure());
};

export const setAnswer = (answer, index) => ({
  type: types.QUIZ_SET_ANSWER,
  answer,
  index,
});

export const restartGame = () => ({
  type: types.QUIZ_RESTART,
});
