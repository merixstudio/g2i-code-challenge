import { combineReducers } from 'redux';
import quiz from './Quiz';

export default combineReducers({
  quiz,
});
