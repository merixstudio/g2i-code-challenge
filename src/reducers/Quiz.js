import * as types from '../types/Quiz';

const initialState = {
  isQuestionsFetching: false,
  questions: [],
  questionIndex: 0,
  score: 0,
};

export default (state = initialState, action) => {
  const { type, payload, answer, index } = action;
  switch (type) {
    case types.QUIZ_QUESTIONS_FETCH_REQUESTED: {
      return {
        ...state,
        isQuestionsFetching: true,
      };
    }
    case types.QUIZ_QUESTIONS_FETCH_FAILURE: {
      return {
        ...state,
        isQuestionsFetching: false,
      };
    }
    case types.QUIZ_QUESTIONS_FETCH_SUCCESSFUL: {
      return {
        ...state,
        isQuestionsFetching: false,
        questions: payload,
      };
    }
    case types.QUIZ_SET_ANSWER: {
      let { questions, questionIndex, score } = state;
      const isCorrect = questions[index].correct_answer === answer;

      questionIndex = index + 1;
      questions = [
        ...questions.slice(0, index),
        {
          ...questions[index],
          isCorrect,
        },
        ...questions.slice(index + 1),
      ];

      if (isCorrect) {
        score += 1;
      }

      return {
        ...state,
        questionIndex,
        questions,
        score,
      }
    }
    case types.QUIZ_RESTART: {
      return {
        ...state,
        questions: [],
        questionIndex: 0,
        score: 0,
      }
    }
    default:
      return state;
  }
}
