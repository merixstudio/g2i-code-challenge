import React from 'react';
import { View, Text } from 'react-native';
import { AllHtmlEntities } from 'html-entities';

const Question = ({ text, isCorrect, isDone }) => {
  const entities = new AllHtmlEntities();
  let { questionStyle, textStyle } = styles;

  if (isDone) {
    if (isCorrect) {
      questionStyle = {
        ...questionStyle,
        borderColor: '#4cab00',
      }
      textStyle = {
        ...textStyle,
        color: '#4cab00',
      }
    } else {
      questionStyle = {
        ...questionStyle,
        borderColor: '#c90000',
      }
      textStyle = {
        ...textStyle,
        color: '#c90000',
      }
    }
  }

  return (
    <View style={questionStyle}>
      <Text style={textStyle}>{entities.decode(text)}</Text>
    </View>
  );
};

const styles = {
  questionStyle: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#007aff',
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 10,
    padding: 20,
  },
  textStyle: {
    fontSize: 16,
  }
}

Question.defaultProps = {
  isDone: false,
  isCorrect: null,
}

export { Question };
