import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as quizActions from '../actions/Quiz';

import { Button } from '../components/common';

class Home extends Component {
  static navigationOptions = {
    title: 'Welcome to the Trivia Challenge!',
  };

  constructor(props) {
    super(props);

    this.navigateToQuiz = this.navigateToQuiz.bind(this);
  }

  navigateToQuiz(evt, type = 'new') {
    const { navigate } = this.props.navigation;

    if (type === 'new') {
      this.props.actions.restartGame();
    }

    return navigate('Quiz');
  }

  renderButtons() {
    const { questionIndex, questions } = this.props;
    const { buttonsStyle } = styles;

    let result = (
      <View style={buttonsStyle}>
        <Button onPress={this.navigateToQuiz}>
          BEGIN
          </Button>
      </View>
    );

    if (questionIndex > 0) {
      result = (
        <View style={buttonsStyle}>
          <Button onPress={this.navigateToQuiz}>
            NEW GAME
          </Button>
          <Button onPress={(evt) => this.navigateToQuiz(evt, 'continue')}>
            CONTINUE
          </Button>
        </View>
      );
    }

    return result;
  }

  render() {
    const {
      containerStyle,
      paragraphsContainerStyle,
      paragraphStyle,
    } = styles;

    return (
      <View style={containerStyle}>
        <View style={paragraphsContainerStyle}>
          <Text style={paragraphStyle}>You will be presented with 10 True or False questions.</Text>
          <Text style={paragraphStyle}>Can you score 100%?</Text>
        </View>
        {this.renderButtons()}
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    height: Dimensions.get('window').height,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  paragraphsContainerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    maxWidth: 200,
  },
  paragraphStyle: {
    fontSize: 20,
    textAlign: 'center',
  },
  buttonsStyle: {
    height: 45,
    width: 300,
    marginBottom: 10,
    flexDirection: 'row',
  }
};


const mapStateToProps = (state) => ({
  questions: state.quiz.questions,
  questionIndex: state.quiz.questionIndex,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    ...quizActions,
  }, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
