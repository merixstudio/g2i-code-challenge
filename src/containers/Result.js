import React, { Component } from 'react';
import { View, Text, Dimensions, FlatList } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as quizActions from '../actions/Quiz';
import { NavigationActions } from 'react-navigation';

import {
  Header,
  Loader,
  Question,
  Button
} from '../components/common/';

class Result extends Component {
  static navigationOptions = {
    title: 'Result',
  };

  constructor(props) {
    super(props);

    this.keyExtractor = this.keyExtractor.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.handlePressButton = this.handlePressButton.bind(this);
  }

  keyExtractor(item) {
    return item.question;
  }

  renderItem({item}) {
    return (
      <Question
        text={item.question}
        isCorrect={item.isCorrect}
        isDone
      />
    )
  }

  handlePressButton() {
    this.props.actions.restartGame();
    const { dispatch } = this.props.navigation;
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Home' })
      ],
    });

    return dispatch(resetAction);
  }

  render() {
    const { isQuestionsFetching, questions, score } = this.props;
    const {
      containerStyle,
      scoreContainerStyle,
      headlineTextStyle,
      scoreTextStyle,
      buttonsStyle
    } = styles;

    return (
      <View style={containerStyle}>
        <View style={scoreContainerStyle}>
          <Text style={headlineTextStyle}>
            You scored
          </Text>
          <Text style={scoreTextStyle}>
            {score} / {questions.length}
          </Text>
        </View>

        <FlatList
          data={questions}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
        />

        <View style={buttonsStyle}>
          <Button onPress={this.handlePressButton}>
            PLAY AGAIN?
          </Button>
        </View>
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  scoreContainerStyle: {
    margin: 15,
    alignItems: 'center',
  },
  headlineTextStyle: {
    fontSize: 18,
  },
  scoreTextStyle: {
    fontSize: 22,
    fontWeight: 'bold',
  },
  buttonsStyle: {
    height: 45,
    width: 300,
    marginBottom: 10,
    marginTop: 10,
  }
};

const mapStateToProps = (state) => ({
  questions: state.quiz.questions,
  questionIndex: state.quiz.questionIndex,
  score: state.quiz.score,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    ...quizActions,
  }, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Result);
