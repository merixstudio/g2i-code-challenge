import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as quizActions from '../actions/Quiz';
import { NavigationActions } from 'react-navigation';

import {
  Header,
  Loader,
  Question,
  Button
} from '../components/common/';

class Quiz extends Component {
  static navigationOptions = {
    title: 'Quiz',
  };

  constructor(props) {
    super(props);

    this.handlePressButton = this.handlePressButton.bind(this);
  }

  componentDidMount() {
    const { questions } = this.props;

    if (!questions.length) {
      this.props.actions.fetchQuizQuestions();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { questions, questionIndex } = nextProps;

    if (questionIndex === questions.length && questionIndex > 0) {
      const { dispatch } = this.props.navigation;
      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'Result' })
        ],
      });

      return dispatch(resetAction);
    }
  }

  handlePressButton(answer) {
    const { questionIndex } = this.props;
    this.props.actions.setAnswer(answer, questionIndex);
  }

  renderQuestion() {
    const { questions, questionIndex } = this.props;
    const {
      containerStyle,
      questionContainerStyle,
      questionStyle,
      buttonsStyle,
      counterStyle,
    } = styles;

    let results = null;
    if (questions.length && questionIndex !== questions.length) {
      const current = questions[questionIndex];
      results = (
        <View style={containerStyle}>
          <Header headerText={current.category} />
          <View style={questionContainerStyle}>
            <Question text={current.question} />

            <View style={buttonsStyle}>
              <Button onPress={() => this.handlePressButton('False')}>False</Button>
              <Button onPress={() => this.handlePressButton('True')}>True</Button>
            </View>

            <Text style={counterStyle}>
              {questionIndex + 1} of {questions.length}
            </Text>
          </View>
        </View>
      );
    }

    return results;
  }

  render() {
    const { isQuestionsFetching, questions } = this.props;
    const { containerStyle } = styles;

    return (
      <View style={{ flex: 1 }}>
        {!isQuestionsFetching ?
          this.renderQuestion() : (
          <View style={containerStyle}>
            <Header headerText="" />
            <Loader size="large" />
          </View>
        )}
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    height: Dimensions.get('window').height - 60,
  },
  questionContainerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonsStyle: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    height: 42,
    marginBottom: 40,
    marginLeft: 30,
    marginRight: 30,
  },
  counterStyle: {
    fontSize: 20,
    fontWeight: 'bold',
  }
};

const mapStateToProps = (state) => ({
  isQuestionsFetching: state.quiz.isQuestionsFetching,
  questions: state.quiz.questions,
  questionIndex: state.quiz.questionIndex,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    ...quizActions,
  }, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Quiz);
