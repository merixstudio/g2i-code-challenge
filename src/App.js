import React from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './store';
import { StackNavigator } from 'react-navigation';

import HomeScreen from './containers/Home';
import QuizScreen from './containers/Quiz';
import ResultScreen from './containers/Result';

const Router = StackNavigator({
  Home: { screen: HomeScreen },
  Quiz: { screen: QuizScreen },
  Result: { screen: ResultScreen },
});

const App = () => {
  const store = configureStore();

  return (
    <Provider store={store}>
      <View style={{ flex: 1 }}>
        <Router />
      </View>
    </Provider>
  );
};

export default App;
